PasswordDesk
============

A cross-platform native desktop client for [Nextcloud Passwords](https://apps.nextcloud.com/apps/passwords).


Current Status
--------------

This project is still under active development. The following features are supported:

- Logging into Nextcloud
- Downloading all passwords and folders
- Storing passwords locally

The following features still need to be developed:

- Creating and updating passwords, see #10
- Encrypt passwords at rest, see #2
- Binary builds, see #6, #7, #8
- Log out automatically, see #1, #5
- [And more...](https://gitlab.com/superfly/passworddesk/-/issues)
