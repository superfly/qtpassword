#include "mainwindow.h"
#include "qglobal.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QHostInfo>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDesktopServices>
#include <QTimer>
#include <QVector>
#include <QStandardPaths>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDateTime>
#include <QLocale>
#include <QVariant>
#include <QGuiApplication>
#include <QClipboard>

#include "passwordsapi.h"
#include "models/folder.h"
#include "models/password.h"

const QString LOGIN_PATH = "/index.php/login/v2";
const QString ROOT_FOLDER = "00000000-0000-0000-0000-000000000000";
const int INITIAL_LOGIN_INTERVAL = 5000;
const int SUBSEQUENT_LOGIN_INTERVAL = 1000;
const int HIDE_MESSAGE_INTERVAL = 5000;


bool isValidUrl(QUrl url)
{
    bool isValid = false;
    if (url.isValid()) {
        isValid = true;
    }
    QHostInfo info = QHostInfo::fromName(url.host());
    if (info.error() != QHostInfo::NoError) {
        isValid = false;
    }
    return isValid;
}

void setRetainSizePolicy(QWidget *widget)
{
    QSizePolicy sp = widget->sizePolicy();
    sp.setRetainSizeWhenHidden(true);
    widget->setSizePolicy(sp);
}

QString dataPath()
{
    QStringList paths = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation);
    return paths.first();
}

void initializeDatabase(QSqlDatabase &db)
{
    QSqlQuery query(db);
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , netManager(new QNetworkAccessManager(this))
    , toolbarSpacer(new QWidget(this))
    , searchEdit(new QLineEdit(this))
    , database(new Database("QSQLITE"))
    , activeFilters(new QList<Filter>())
{
    ui->setupUi(this);
    toolbarSpacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    ui->toolBar->addWidget(toolbarSpacer);
    searchEdit->setClearButtonEnabled(true);
    searchEdit->setPlaceholderText(tr("Search..."));
    ui->toolBar->addWidget(searchEdit);
    if (!database->initialize()) {
        showLoginError(database->error());
    }
    connect(netManager, &QNetworkAccessManager::finished, this, &MainWindow::onRequestFinished);
    connect(searchEdit, &QLineEdit::textChanged, this, &MainWindow::on_searchEdit_textChanged);
    if (settings.value("server/url", "").toString().isEmpty() || settings.value("server/username", "").toString().isEmpty() || settings.value("server/password").toString().isEmpty())
    {
        isLoggedIn = false;
        updateActions();
        startLogin();
    }
    else
    {
        isLoggedIn = true;
        updateActions();
        if (settings.value("storage/refresh-automatically", true).toBool()) {
            updatePasswordsFromServer();
        }
        else {
            populateFolders();
            ui->pagesWidget->setCurrentWidget(ui->passwordsPage);
            QTreeWidgetItem* allFoldersItem = ui->sectionsTreeWidget->findItems(tr("All Passwords"), Qt::MatchExactly).first();
            ui->sectionsTreeWidget->setCurrentItem(allFoldersItem);
        }
    }
}

MainWindow::~MainWindow()
{
    delete database;
    delete toolbarSpacer;
    delete searchEdit;
    delete netManager;
    delete ui;
}


void MainWindow::on_loginButton_clicked()
{
    QUrl url = QUrl::fromUserInput(ui->serverEdit->text());
    if (!isValidUrl(url))
    {
        QMessageBox::critical(this, tr("Invalid Server"), tr("Your server URL is not correct"));
        return;
    }
    serverUrl = url;
    ui->statusBar->showMessage(tr("Logging you in..."));
    ui->loginProgressBar->show();
    ui->copyLinkButton->show();
    ui->serverEdit->setDisabled(true);
    ui->loginButton->setDisabled(true);
    netManager->post(createLoginRequest(serverUrl), QByteArray());
}

void MainWindow::on_sectionsTreeWidget_itemSelectionChanged()
{
    QList<QTreeWidgetItem*> selectedItems = ui->sectionsTreeWidget->selectedItems();
    if (!selectedItems.isEmpty()) {
        const QTreeWidgetItem* item = selectedItems.first();
        bool isValidQuery = true;
        activeFilters->clear();
        if (item->text(0) == tr("All Passwords")) {
            // just skip this one, because we want everything, but we need a catchall for folders
        }
        else if (item->text(0) == tr("Favorites")) {
            activeFilters->append(Filter("favorite", QVariant(true)));
        }
        else if (item->text(0) == tr("Shares")) {
            activeFilters->append(Filter("shared", QVariant(true)));
        }
        else if (item->text(0) == tr("Trash")) {
            activeFilters->append(Filter("trashed", QVariant(true)));
        }
        else if (item->text(0) == tr("Security")) {
            activeFilters->append(Filter("status", QVariant(API::Password::GOOD), "<>"));
        }
        else {
            QVariant data = item->data(0, Qt::UserRole);
            if (data.isValid()) {
                activeFilters->append(Filter("folder", data));
            }
            else {
                isValidQuery = false;
            }
        }
        if (isValidQuery) {
            loadPasswords();
        }
        else {
            clearPasswords();
        }
    }
}

void MainWindow::clearPasswords()
{
    for (QTreeWidgetItem *child : ui->passwordTreeWidget->invisibleRootItem()->takeChildren())
    {
        delete child;
    }
}

void MainWindow::loadPasswords()
{
    QList<Filter> filters(*activeFilters);
    if (!searchEdit->text().isEmpty()) {
        filters.append(Filter("label", QVariant("%" + searchEdit->text() + "%"), "LIKE"));
        // filters.append(Filter("username", QVariant("%" + searchEdit->text() + "%"), "LIKE"));
    }
    QList<Password> passwords = database->getPasswords(filters);
    clearPasswords();
    for (Password password : qAsConst(passwords)) {
        QTreeWidgetItem *item = new QTreeWidgetItem();
        item->setIcon(0, QIcon(":/icons/password.svg"));
        item->setText(1, password.label());
        item->setText(2, password.username());
        if (password.status() == API::Password::GOOD) {
            item->setIcon(3, QIcon(":/icons/status-good.svg"));
            item->setStatusTip(3, tr("Good"));
            item->setToolTip(3, tr("Good"));
        }
        else if (password.status() == API::Password::DUPLICATE) {
            item->setIcon(3, QIcon(":/icons/status-duplicate.svg"));
            item->setStatusTip(3, tr("Duplicate"));
            item->setToolTip(3, tr("Duplicate"));
        }
        else if (password.status() == API::Password::BREACHED) {
            item->setIcon(3, QIcon(":/icons/status-breached.svg"));
            item->setStatusTip(3, tr("Breached"));
            item->setToolTip(3, tr("Breached"));
        }
        item->setText(4, password.updated().toString(QLocale::system().dateTimeFormat(QLocale::ShortFormat)));
        if (password.isFavorite()) {
            item->setIcon(5, QIcon(":/icons/favorites.svg"));
        }
        item->setData(0, Qt::UserRole, QVariant(password.id()));
        ui->passwordTreeWidget->invisibleRootItem()->addChild(item);

    }
    ui->passwordTreeWidget->resizeColumnToContents(0);
    ui->passwordTreeWidget->resizeColumnToContents(1);
    ui->passwordTreeWidget->resizeColumnToContents(2);
    ui->passwordTreeWidget->resizeColumnToContents(3);
    ui->passwordTreeWidget->resizeColumnToContents(4);
    ui->passwordTreeWidget->resizeColumnToContents(5);
}

void MainWindow::on_aboutAction_triggered()
{
    QMessageBox::about(this, tr("About PasswordDesk"), tr("PasswordDesk is a cross-platform native desktop client for the Nextcloud Passwords app."));
}

void MainWindow::on_exitAction_triggered()
{
    close();
}

void MainWindow::on_logoutAction_triggered()
{
    settings.remove("server/url");
    settings.remove("server/username");
    settings.remove("server/password");
    isLoggedIn = false;
    updateActions();
    startLogin();
}

void MainWindow::on_passwordTreeWidget_itemActivated(QTreeWidgetItem *item, int column)
{
    QString passwordId = item->data(0, Qt::UserRole).toString();
    Password password = database->getPassword(passwordId);
    setEditFields(
        password.label(),
        password.username(),
        password.password(),
        password.url(),
        password.notes()
    );
    ui->pagesWidget->setCurrentWidget(ui->editPage);
}

void MainWindow::on_showPasswordButton_clicked()
{
    if (ui->passwordEdit->echoMode() == QLineEdit::Password) {
        ui->passwordEdit->setEchoMode(QLineEdit::Normal);
    }
    else {
        ui->passwordEdit->setEchoMode(QLineEdit::Password);
    }
}

void MainWindow::on_copyLinkButton_clicked()
{
    QClipboard *clipboard = QGuiApplication::clipboard();
    clipboard->setText(loginUrl.toString());
    ui->statusBar->showMessage(tr("Copied login link to clipboard"), HIDE_MESSAGE_INTERVAL);
}

void MainWindow::updateActions()
{
    ui->logoutAction->setEnabled(isLoggedIn && !isRefreshing);
    ui->refreshPasswordsAction->setEnabled(isLoggedIn && !isRefreshing);
    QList<QTreeWidgetItem*> selectedItems = ui->passwordTreeWidget->selectedItems();
    ui->editAction->setEnabled(!selectedItems.empty());
}

QNetworkRequest MainWindow::createRequest(QUrl url, QString path, bool isJson)
{
    if (!path.isEmpty()) {
        if (url.path().isEmpty()) {
            url.setPath(path);
        }
        else {
            url.setPath(url.path() + path);
        }
    }
    QNetworkRequest request(url);
    if (isJson) {
        request.setRawHeader(QByteArray("Content-Type"), QByteArray("application/json"));
    }
    request.setRawHeader(QByteArray("Accept"), QByteArray("application/json"));
    request.setRawHeader(QByteArray("User-Agent"), QByteArray("PasswordDesk/0.1"));
    if (hasCredentials())
    {
        request.setRawHeader(QByteArray("Authorization"), QByteArray("Basic ") + encodeCredentials());
    }
    if (!sessionId.isEmpty())
    {
        request.setRawHeader(QByteArray("X-API-SESSION"), sessionId);
    }
    return request;
}

QNetworkRequest MainWindow::createLoginRequest(QUrl url)
{
    if (url.scheme().isEmpty() || url.scheme() != "https") {
        url.setScheme("https");
    }
    return createRequest(url, LOGIN_PATH);
}

bool MainWindow::hasCredentials()
{
    return !settings.value("server/username", "").toString().isEmpty() && !settings.value("server/password", "").toString().isEmpty();
}

QByteArray MainWindow::encodeCredentials()
{
    QByteArray creds = settings.value("server/username").toByteArray() + ":" + settings.value("server/password").toByteArray();
    return creds.toBase64();
}

void MainWindow::onRequestFinished(QNetworkReply *reply)
{
    if (reply->hasRawHeader("X-API-SESSION"))
    {
        sessionId = reply->rawHeader("X-API-SESSION");
    }
    if (reply->url().path().endsWith(LOGIN_PATH))
    {
        handleLogin(reply);
    }
    else if (reply->url() == pollEndpoint) {
        handlePoll(reply);
    }
    else if (reply->url().toString().endsWith(API::Session::REQUEST))
    {
        handleRequestSession(reply);
    }
    else if (reply->url().toString().endsWith(API::Session::OPEN))
    {
        handleOpenSession(reply);
    }
    else if (reply->url().toString().endsWith(API::Folder::LIST))
    {
        handleGetFolders(reply);
    }
    reply->deleteLater();
}

void MainWindow::on_searchEdit_textChanged(const QString &text)
{
    loadPasswords();
}

void MainWindow::startLogin()
{
    serverUrl = QString();
    setRetainSizePolicy(ui->loginProgressBar);
    setRetainSizePolicy(ui->copyLinkButton);
    ui->loginProgressBar->hide();
    ui->copyLinkButton->hide();
    ui->statusBar->showMessage(tr("You are not logged in. Please log in to continue."));
    ui->pagesWidget->setCurrentWidget(ui->loginPage);
}

void MainWindow::showLoginError(QString errorText)
{
    ui->statusBar->showMessage(errorText);
    ui->loginProgressBar->hide();
    ui->serverEdit->setDisabled(false);
    ui->loginButton->setDisabled(false);
}

void MainWindow::handleLogin(QNetworkReply *reply)
{
    if (reply->error() != QNetworkReply::NoError)
    {
        showLoginError(reply->errorString());
        return;
    }
    QByteArray responseRaw = reply->readAll();
    QJsonDocument response = QJsonDocument::fromJson(responseRaw);
    QJsonObject json = response.object();
    if (json.contains("login") && json.contains("poll"))
    {
        pollEndpoint = QUrl::fromUserInput(json["poll"].toObject()["endpoint"].toString());
        pollToken = json["poll"].toObject()["token"].toString().toUtf8();
        loginUrl = QUrl(json["login"].toString());
        QDesktopServices::openUrl(loginUrl);
        QTimer::singleShot(INITIAL_LOGIN_INTERVAL, this, &MainWindow::checkLoginPoll);
    }
    else
    {
        showLoginError(tr("Unable to start the login process"));
    }
}

void MainWindow::handlePoll(QNetworkReply *reply)
{
    if (reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() != 200) {
        QTimer::singleShot(SUBSEQUENT_LOGIN_INTERVAL, this, &MainWindow::checkLoginPoll);
        return;
    }
    if (reply->error() != QNetworkReply::NoError)
    {
        showLoginError(reply->errorString());
        return;
    }
    QJsonObject json = QJsonDocument::fromJson(reply->readAll()).object();
    if (json.contains("server") && json.contains("loginName") && json.contains("appPassword"))
    {
        pollEndpoint = QUrl();
        pollToken = QByteArray();
        settings.setValue("server/url", json["server"].toString());
        settings.setValue("server/username", json["loginName"].toString());
        settings.setValue("server/password", json["appPassword"].toString());
        isLoggedIn = true;
        updateActions();
        updatePasswordsFromServer();
    }
    else
    {
        showLoginError(tr("There was an error while logging in, please try again."));
    }
}

void MainWindow::checkLoginPoll()
{
    if (!pollToken.isEmpty())
    {
        QNetworkRequest request = createRequest(pollEndpoint);
        QByteArray body = "token=" + pollToken;
        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
        netManager->post(request, QString("token=" + pollToken).toUtf8());
    }
}

void MainWindow::updatePasswordsFromServer()
{
    isRefreshing = true;
    updateActions();
    ui->pagesWidget->setCurrentWidget(ui->refreshingPage);
    showPasswordsInfo(tr("Requesting session..."));
    QNetworkRequest request = createRequest(QUrl(settings.value("server/url").toString()), API::BASE_URL + API::Session::REQUEST);
    currentReply = netManager->get(request);
}

void MainWindow::showPasswordsInfo(QString infoText)
{
    ui->statusBar->showMessage(infoText);
}

void MainWindow::showPasswordsError(QString errorText)
{
    ui->statusBar->showMessage(errorText, HIDE_MESSAGE_INTERVAL);
}

void MainWindow::hidePasswordsMessage()
{
    ui->statusBar->clearMessage();
}

void MainWindow::handleRequestSession(QNetworkReply *reply)
{
    if (reply->error() != QNetworkReply::NoError)
    {
        showPasswordsError(reply->errorString());
        return;
    }
    QByteArray rawResponse = reply->readAll();
    QJsonObject json = QJsonDocument::fromJson(rawResponse).object();
    if (json.contains("challenge") || json.contains("token"))
    {
        showPasswordsInfo(tr("Opening session..."));
        QNetworkRequest request = createRequest(QUrl(settings.value("server/url").toString()), API::BASE_URL + API::Session::OPEN);
        netManager->post(request, "challenge");
    }
    else
    {
        showPasswordsInfo(tr("Fetching folders and passwords..."));
        QNetworkRequest request = createRequest(QUrl(settings.value("server/url").toString()), API::BASE_URL + API::Folder::LIST, true);
        currentReply = netManager->post(request, QByteArray("{\"details\": \"model+passwords\"}"));
    }
}

void MainWindow::handleOpenSession(QNetworkReply *reply)
{
    if (reply->error() != QNetworkReply::NoError)
    {
        showPasswordsError(reply->errorString());
        return;
    }
    showPasswordsInfo(tr("Fetching folders and passwords..."));
    QNetworkRequest request = createRequest(QUrl(settings.value("server/url").toString()), API::BASE_URL + API::Folder::LIST);
    netManager->post(request, QString("details=model+passwords").toUtf8());
}

void MainWindow::handleGetFolders(QNetworkReply *reply)
{
    if (reply->error() != QNetworkReply::NoError)
    {
        showPasswordsError(reply->errorString());
        ui->pagesWidget->setCurrentWidget(ui->passwordsPage);
        return;
    }
    QByteArray rawResponse = reply->readAll();
    QJsonArray jsonArray = QJsonDocument::fromJson(rawResponse).array();
    if (!jsonArray.isEmpty())
    {
        showPasswordsInfo(tr("Updating local passwords..."));
        for (QJsonValue value : qAsConst(jsonArray))
        {
            QApplication::processEvents();
            QJsonObject folderObj = value.toObject();
            QJsonArray passwords = folderObj["passwords"].toArray();
            Folder folder(folderObj);
            database->saveFolder(folder);
            for (QJsonValue value : qAsConst(passwords))
            {
                QApplication::processEvents();
                Password password(value.toObject());
                database->savePassword(password);
            }
            database->commit();
        }
        populateFolders();
    }
    hidePasswordsMessage();
    isRefreshing = false;
    updateActions();
    ui->pagesWidget->setCurrentWidget(ui->passwordsPage);
    if (ui->sectionsTreeWidget->selectedItems().empty()) {
        QTreeWidgetItem* allFoldersItem = ui->sectionsTreeWidget->findItems(tr("All Passwords"), Qt::MatchExactly).first();
        ui->sectionsTreeWidget->setCurrentItem(allFoldersItem);
    }
}

void MainWindow::populateFolderChildren(QTreeWidgetItem *parentItem, QString parentFolder)
{
    QList<Folder> folders = database->getFolders(parentFolder);
    for (Folder folder : folders) {
        QTreeWidgetItem *item = new QTreeWidgetItem();
        item->setText(0, folder.label());
        item->setIcon(0, QIcon(":/icons/folder.svg"));
        item->setData(0, Qt::UserRole, folder.id());
        parentItem->addChild(item);
        populateFolderChildren(item, folder.id());
    }
}

void MainWindow::populateFolders()
{
    // clear any folders in the tree
    QTreeWidgetItem *foldersItem = ui->sectionsTreeWidget->findItems(tr("Folders"), Qt::MatchExactly).first();
    // Loop through the children and remove them
    if (foldersItem->childCount() > 0)
    {
        for (QTreeWidgetItem *child : foldersItem->takeChildren())
        {
            delete child;
        }
    }
    populateFolderChildren(foldersItem, ROOT_FOLDER);
    foldersItem->setExpanded(true);
}

void MainWindow::setEditFields(QString title, QString username, QString password, QString url, QString notes)
{
    ui->titleEdit->setText(title);
    ui->usernameEdit->setText(username);
    ui->passwordEdit->setText(password);
    ui->urlEdit->setText(url);
    ui->notesEdit->setPlainText(notes);
}

void MainWindow::on_refreshPasswordsAction_triggered()
{
    updatePasswordsFromServer();
}

void MainWindow::on_settingsAction_triggered()
{
    ui->logOutOnExitCheckBox->setChecked(settings.value("security/log-out", false).toBool());
    ui->storePasswordsLocallyCheckBox->setChecked(settings.value("storage/store-passwords-locally", true).toBool());
    ui->refreshPasswordsOnStartupCheckBox->setChecked(settings.value("storage/refresh-automatically", true).toBool());
    ui->pagesWidget->setCurrentWidget(ui->settingsPage);
}

void MainWindow::on_settingsButtonBox_rejected()
{
    ui->pagesWidget->setCurrentWidget(ui->passwordsPage);
}

void MainWindow::on_lockAutomaticallyCheckBox_stateChanged(int state)
{
    ui->lockTimeoutLabel->setEnabled(state == Qt::Checked);
    ui->lockTimeoutSpinBox->setEnabled(state == Qt::Checked);
}

void MainWindow::on_settingsButtonBox_accepted()
{
    settings.setValue("security/log-out", ui->logOutOnExitCheckBox->isChecked());
    settings.setValue("storage/store-passwords-locally", ui->storePasswordsLocallyCheckBox->isChecked());
    settings.setValue("storage/refresh-automatically", ui->refreshPasswordsOnStartupCheckBox->isChecked());
    settings.sync();
    ui->pagesWidget->setCurrentWidget(ui->passwordsPage);
}

void MainWindow::on_editButtonBox_accepted()
{
    ui->pagesWidget->setCurrentWidget(ui->passwordsPage);
    setEditFields("", "", "", "", "");
}


void MainWindow::on_editButtonBox_rejected()
{
    ui->pagesWidget->setCurrentWidget(ui->passwordsPage);
    setEditFields("", "", "", "", "");
}


void MainWindow::on_passwordTreeWidget_itemSelectionChanged()
{
    updateActions();
}

