#include "database.h"

const QList<QString> FOLDER_COLUMNS = {
    "id",
    "revision",
    "parent",
    "label",
    "created",
    "edited",
    "updated",
    "favorite",
    "hidden",
    "trashed",
    "cse_key",
    "cse_type",
    "sse_type"
};
const QList<QString> PASSWORD_COLUMNS = {
    "id",
    "hash",
    "revision",
    "folder",
    "label",
    "username",
    "password",
    "url",
    "notes",
    "share",
    "created",
    "edited",
    "updated",
    "editable",
    "favorite",
    "hidden",
    "shared",
    "trashed",
    "status",
    "status_code",
    "custom_fields",
    "cse_key",
    "cse_type",
    "sse_type"
};
const QMap<QString, QString> COLUMN_CONVERSION = {
    {"cseKey", "cse_key"},
    {"cseType", "cse_type"},
    {"sseType", "sse_type"},
    {"statusCode", "status_code"},
    {"customFields", "custom_fields"}
};
const QMap<QString, QString> GET_TABLE_SQL = {
    {"QSQLITE", "SELECT COUNT(name) FROM sqlite_schema WHERE type = 'table' AND name = '%1'"},
    {"QMYSQL", "SELECT COUNT(table_name) FROM information_schema.tables WHERE table_name = '%1'"},
    {"QPOSTGRESQL", "SELECT COUNT(table_name) FROM information_schema.tables WHERE table_schema = 'public' AND table_type = 'BASE TABLE' AND table_name = '%1'"}
};
const QString CREATE_FOLDERS_SQL = "\
  CREATE TABLE folders ( \
    id VARCHAR(255) PRIMARY KEY, \
    revision VARCHAR(255), \
    parent VARCHAR(255), \
    label VARCHAR(255), \
    created INTEGER, \
    edited INTEGER, \
    updated INTEGER, \
    favorite BOOL, \
    hidden BOOL, \
    trashed BOOL, \
    cse_key VARCHAR(255), \
    cse_type VARCHAR(255), \
    sse_type VARCHAR(255), \
    FOREIGN KEY(parent) REFERENCES folder(id) \
  );";
const QString CREATE_PASSWORDS_SQL = "\
  CREATE TABLE passwords ( \
    id VARCHAR(255) PRIMARY KEY, \
    hash VARCHAR(255), \
    revision VARCHAR(255), \
    folder VARCHAR(255), \
    label VARCHAR(255), \
    username VARCHAR(255), \
    password VARCHAR(255), \
    url VARCHAR(255), \
    notes VARCHAR(255), \
    share VARCHAR(255), \
    created INTEGER, \
    edited INTEGER, \
    updated INTEGER, \
    editable BOOL, \
    favorite BOOL, \
    hidden BOOL, \
    shared BOOL, \
    trashed BOOL, \
    status INTEGER, \
    status_code VARCHAR(255), \
    custom_fields VARCHAR(255), \
    cse_key VARCHAR(255), \
    cse_type VARCHAR(255), \
    sse_type VARCHAR(255), \
    FOREIGN KEY(folder) REFERENCES folder(id) \
  );";

QSqlQuery insertSql(QString tableName, QMap<QString, QVariant> args)
{
    QSqlQuery query;
    QList<QString> keys = args.keys();
    QString columnNames;
    QString arguments;
    for (const QString &key : keys) {
        if (columnNames.isEmpty()) {
            columnNames = key;
        }
        else {
            columnNames += QString(", %1").arg(key);
        }
        if (arguments.isEmpty()) {
            arguments = QString(":%1").arg(key);
        }
        else {
            arguments += QString(", :%1").arg(key);
        }
    }
    QString queryString = QString("INSERT INTO %1 (%2) VALUES (%3);").arg(tableName).arg(columnNames).arg(arguments);
    query.prepare(queryString);
    for (const QString &key : keys) {
        query.bindValue(QString(":%1").arg(key), args.value(key));
    }
    return query;
}

QSqlQuery updateSql(QString tableName, QString id, QMap<QString, QVariant> args)
{
    QSqlQuery query;
    QList<QString> keys = args.keys();
    QString columnNames;
    for (const QString &key : keys) {
        if (columnNames.isEmpty()) {
            columnNames = QString("%1 = :%1").arg(key);
        }
        else {
            columnNames += QString(", %1 = %1").arg(key);
        }
    }
    QString queryString = QString("UPDATE %1 SET %2 WHERE id = %3;").arg(tableName).arg(columnNames).arg(id);
    query.prepare(queryString);
    for (const QString &key : keys) {
        query.bindValue(QString(":%1").arg(key), args.value(key));
    }
    return query;
}

Database::Database(QString dbtype)
{
    _dbtype = dbtype;
    _error = "";
}

bool Database::initialize()
{
    QSqlDatabase db = QSqlDatabase::addDatabase(_dbtype);
    QDir baseDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    if (!baseDir.exists()) {
        baseDir.mkpath(".");
    }
    db.setDatabaseName(baseDir.filePath("ncpasswords.sqlite"));
    if (db.open()) {
        QSqlQuery query;
        bool ok = false;
        ok = query.exec(GET_TABLE_SQL[_dbtype].arg("folders"));
        if (!ok) {
            _error = query.lastError().text();
            return false;
        }
        query.first();
        if (query.record().field(0).value().toInt() == 0) {
            query.exec(CREATE_FOLDERS_SQL);
        }
        ok = query.exec(GET_TABLE_SQL[_dbtype].arg("passwords"));
        if (!ok) {
            _error = query.lastError().text();
            return false;
        }
        query.next();
        if (query.record().field(0).value().toInt() == 0) {
            query.exec(CREATE_PASSWORDS_SQL);
        }
        return true;
    }
    else
    {
        _error = db.lastError().text();
        return false;
    }
}

QString Database::error()
{
    return _error;
}

bool Database::exists(QString tableName, QString id)
{
    QSqlQuery query;
    query.prepare(QString("SELECT COUNT(id) FROM %1 WHERE id = :id").arg(tableName));
    query.bindValue(":id", id);
    query.exec();
    query.next();
    int count = query.record().field(0).value().toInt();
    return count > 0;
}

bool Database::insertRecord(QString tableName, QMap<QString, QVariant> record)
{
    QMap<QString, QVariant> args;
    for (auto i = record.constBegin(), end = record.constEnd(); i != end; ++i) {
        QString key = i.key();
        if (COLUMN_CONVERSION.contains(key)) {
            key = COLUMN_CONVERSION[key];
        }
        if (FOLDER_COLUMNS.contains(key) || PASSWORD_COLUMNS.contains(key)) {
            args[key] = i.value();
        }
    }
    QSqlQuery query = insertSql(tableName, args);
    query.exec();
    return query.lastError().type() == QSqlError::NoError;
}

bool Database::updateRecord(QString tableName, QMap<QString, QVariant> record)
{
    QMap<QString, QVariant> args;
    for (auto i = record.constBegin(), end = record.constEnd(); i != end; ++i) {
        QString key = i.key();
        if (COLUMN_CONVERSION.contains(key)) {
            key = COLUMN_CONVERSION[key];
        }
        if (FOLDER_COLUMNS.contains(key) || PASSWORD_COLUMNS.contains(key)) {
            args[key] = i.value();
        }
    }
    QSqlQuery query = updateSql(tableName, record["id"].toString(), args);
    query.exec();
    return query.lastError().type() == QSqlError::NoError;
}

void Database::commit()
{
    QSqlQuery query;
    query.exec("COMMIT");
}

QList<Folder> Database::getFolders(QString parent)
{
    QList<Folder> folders;
    QSqlQuery query;
    query.prepare("SELECT * FROM folders WHERE parent = :parent ORDER BY label ASC");
    query.bindValue(":parent", parent);
    query.exec();
    while (query.next()) {
        folders.append(Folder(query.record()));
    }
    return folders;
}

bool Database::saveFolder(Folder &folder)
{
    if (exists("folders", folder.id())) {
        return updateRecord("folders", folder.toMap());
    }
    else {
        return insertRecord("folders", folder.toMap());
    }
}

QList<Password> Database::getPasswords(QList<Filter> &filters)
{
    QList<Password> passwords;
    QSqlQuery query;
    if (filters.count() > 0) {
        QString whereClause = "";
        for (Filter filter : filters) {
            if (!filter.field().isEmpty()) {
                if (!whereClause.isEmpty()) {
                    whereClause += " AND ";
                }
                whereClause += QString("%1 %2 :%1").arg(filter.field()).arg(filter.op());
            }
        }
        query.prepare(QString("SELECT * FROM passwords WHERE %1 ORDER BY label").arg(whereClause));
        for (Filter filter : filters) {
            query.bindValue(QString(":%1").arg(filter.field()), filter.value());
        }
    }
    else {
        query.prepare("SELECT * FROM passwords ORDER BY label");
    }
    query.exec();
    while (query.next()) {
        passwords.append(Password(query.record()));
    }
    return passwords;
}

Password Database::getPassword(QString id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM passwords WHERE id = :id");
    query.bindValue(":id", id);
    query.exec();
    query.next();
    return Password(query.record());
}

bool Database::savePassword(Password &password)
{
    if (exists("passwords", password.id())) {
        return updateRecord("passwords", password.toMap());
    }
    else {
        return insertRecord("passwords", password.toMap());
    }
}


Filter::Filter(QString field, QVariant value, QString op)
{
    _field = field;
    _value = value;
    _op = op;
}

QString Filter::field()
{
    return _field;
}

void Filter::setField(QString field)
{
    _field = field;
}

QVariant Filter::value()
{
    return _value;
}

void Filter::setValue(QVariant value)
{
    _value = value;
}

QString Filter::op()
{
    return _op;
}

void Filter::setOp(QString op)
{
    _op = op;
}
