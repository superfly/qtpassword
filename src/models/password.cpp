#include "password.h"

Password::Password() : BaseModel()
{
}

Password::Password(QJsonObject json) : BaseModel(json)
{
    if (json.contains("folder")) {
        setFolder(json["folder"].toString());
    }
    if (json.contains("username")) {
        setUsername(json["username"].toString());
    }
    if (json.contains("password")) {
        setPassword(json["password"].toString());
    }
    if (json.contains("url")) {
        setUrl(json["url"].toString());
    }
    if (json.contains("notes")) {
        setNotes(json["notes"].toString());
    }
    if (json.contains("status")) {
        setStatus(json["status"].toInt());
    }
    if (json.contains("statusCode")) {
        setStatusCode(json["statusCode"].toString());
    }
    if (json.contains("hash")) {
        setHash(json["hash"].toString());
    }
    if (json.contains("share")) {
        setShare(json["share"].toString());
    }
    if (json.contains("shared")) {
        setShared(json["shared"].toBool());
    }
    if (json.contains("editable")) {
        setEditable(json["editable"].toBool());
    }
}

Password::Password(QSqlRecord record) : BaseModel(record)
{
    if (record.contains("folder")) {
        setFolder(record.value("folder").toString());
    }
    if (record.contains("username")) {
        setUsername(record.value("username").toString());
    }
    if (record.contains("password")) {
        setPassword(record.value("password").toString());
    }
    if (record.contains("url")) {
        setUrl(record.value("url").toString());
    }
    if (record.contains("notes")) {
        setNotes(record.value("notes").toString());
    }
    if (record.contains("status")) {
        setStatus(record.value("status").toInt());
    }
    if (record.contains("statusCode")) {
        setStatusCode(record.value("statusCode").toString());
    }
    if (record.contains("hash")) {
        setHash(record.value("hash").toString());
    }
    if (record.contains("share")) {
        setShare(record.value("share").toString());
    }
    if (record.contains("shared")) {
        setShared(record.value("shared").toBool());
    }
    if (record.contains("editable")) {
        setEditable(record.value("editable").toBool());
    }
}

QMap<QString, QVariant> Password::toMap()
{
    QMap<QString, QVariant> map = BaseModel::toMap();
    map["folder"] = QVariant(folder());
    map["username"] = QVariant(username());
    map["password"] = QVariant(password());
    map["url"] = QVariant(url());
    map["notes"] = QVariant(notes());
    map["status"] = QVariant(status());
    map["status_code"] = QVariant(statusCode());
    map["hash"] = QVariant(hash());
    map["share"] = QVariant(share());
    map["shared"] = QVariant(isShared());
    map["editable"] = QVariant(isEditable());
    return map;
}

QString Password::folder()
{
    return _folder;
}

void Password::setFolder(QString folder)
{
    _folder = folder;
}

QString Password::username()
{
    return _username;
}

void Password::setUsername(QString username)
{
    _username = username;
}

QString Password::password()
{
    return _password;
}
void Password::setPassword(QString password)
{
    _password = password;
}
QString Password::url()
{
    return _url;
}
void Password::setUrl(QString url)
{
    _url = url;
}
QString Password::notes()
{
    return _notes;
}
void Password::setNotes(QString notes)
{
    _notes = notes;
}
int Password::status()
{
    return _status;
}
void Password::setStatus(int status)
{
    _status = status;
}
QString Password::statusCode()
{
    return _statusCode;
}
void Password::setStatusCode(QString statusCode)
{
    _statusCode = statusCode;
}
QString Password::hash()
{
    return _hash;
}
void Password::setHash(QString hash)
{
    _hash = hash;
}
QString Password::share()
{
    return _share;
}
void Password::setShare(QString share)
{
    _share = share;
}
bool Password::isShared()
{
    return _shared;
}
void Password::setShared(bool isShared)
{
    _shared = isShared;
}
bool Password::isEditable()
{
    return _editable;
}
void Password::setEditable(bool isEditable)
{
    _editable = isEditable;
}
