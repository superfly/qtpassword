#include "basemodel.h"

BaseModel::BaseModel()
{

}

BaseModel::BaseModel(QJsonObject json)
{
    if (json.contains("id"))
    {
        setId(json["id"].toString());
    }
    if (json.contains("revision"))
    {
        setRevision(json["revision"].toString());
    }
    if (json.contains("label"))
    {
        setLabel(json["label"].toString());
    }
    if (json.contains("cseType"))
    {
        setCseType(json["cseType"].toString());
    }
    if (json.contains("cseKey"))
    {
        setCseKey(json["cseKey"].toString());
    }
    if (json.contains("sseType"))
    {
        setSseType(json["sseType"].toString());
    }
    if (json.contains("hidden"))
    {
        setHidden(json["hidden"].toBool());
    }
    if (json.contains("trashed"))
    {
        setTrashed(json["trashed"].toBool());
    }
    if (json.contains("favorite"))
    {
        setFavorite(json["favorite"].toBool());
    }
    if (json.contains("created"))
    {
        setCreated(QDateTime::fromSecsSinceEpoch(json["created"].toInt()));
    }
    if (json.contains("edited"))
    {
        setCreated(QDateTime::fromSecsSinceEpoch(json["edited"].toInt()));
    }
    if (json.contains("updated"))
    {
        setCreated(QDateTime::fromSecsSinceEpoch(json["updated"].toInt()));
    }
}

BaseModel::BaseModel(QSqlRecord record)
{
    if (record.contains("id"))
    {
        setId(record.value("id").toString());
    }
    if (record.contains("revision"))
    {
        setRevision(record.value("revision").toString());
    }
    if (record.contains("label"))
    {
        setLabel(record.value("label").toString());
    }
    if (record.contains("cseType"))
    {
        setCseType(record.value("cseType").toString());
    }
    if (record.contains("cseKey"))
    {
        setCseKey(record.value("cseKey").toString());
    }
    if (record.contains("sseType"))
    {
        setSseType(record.value("sseType").toString());
    }
    if (record.contains("hidden"))
    {
        setHidden(record.value("hidden").toBool());
    }
    if (record.contains("trashed"))
    {
        setTrashed(record.value("trashed").toBool());
    }
    if (record.contains("favorite"))
    {
        setFavorite(record.value("favorite").toBool());
    }
    if (record.contains("created"))
    {
        setCreated(QDateTime::fromSecsSinceEpoch(record.value("created").toInt()));
    }
    if (record.contains("edited"))
    {
        setCreated(QDateTime::fromSecsSinceEpoch(record.value("edited").toInt()));
    }
    if (record.contains("updated"))
    {
        setCreated(QDateTime::fromSecsSinceEpoch(record.value("updated").toInt()));
    }
}

QMap<QString, QVariant> BaseModel::toMap()
{
    QMap<QString, QVariant> map;
    map["id"] = QVariant(id());
    map["label"] = QVariant(label());
    map["revision"] = QVariant(revision());
    map["cse_type"] = QVariant(cseType());
    map["cse_key"] = QVariant(cseKey());
    map["sse_type"] = QVariant(sseType());
    map["hidden"] = QVariant(isHidden());
    map["trashed"] = QVariant(isTrashed());
    map["favorite"] = QVariant(isFavorite());
    map["created"] = QVariant(created());
    map["edited"] = QVariant(edited());
    map["updated"] = QVariant(updated());
    return map;
}

QVariant BaseModel::operator[](const QString &name)
{
    QMap<QString, QVariant> map = toMap();
    return map.value(name);
}

QString BaseModel::id()
{
    return _id;
}

void BaseModel::setId(QString id)
{
    _id = id;
}

QString BaseModel::label()
{
    return _label;
}

void BaseModel::setLabel(QString label)
{
    _label = label;
}

QString BaseModel::revision()
{
    return _revision;
}

void BaseModel::setRevision(QString revision)
{
    _revision = revision;
}

QString BaseModel::cseType()
{
    return _cseType;
}

void BaseModel::setCseType(QString cseType)
{
    _cseType = cseType;
}

QString BaseModel::cseKey()
{
    return _cseKey;
}

void BaseModel::setCseKey(QString cseKey)
{
    _cseKey = cseKey;
}

QString BaseModel::sseType()
{
    return _sseType;
}

void BaseModel::setSseType(QString sseType)
{
    _sseType = sseType;
}

bool BaseModel::isHidden()
{
    return _hidden;
}

void BaseModel::setHidden(bool isHidden)
{
    _hidden = isHidden;
}

bool BaseModel::isTrashed()
{
    return _trashed;
}

void BaseModel::setTrashed(bool isTrashed)
{
    _trashed = isTrashed;
}

bool BaseModel::isFavorite()
{
    return _favorite;
}

void BaseModel::setFavorite(bool isFavorite)
{
    _favorite = isFavorite;
}

QDateTime BaseModel::edited()
{
    return _edited;
}

void BaseModel::setEdited(QDateTime edited)
{
    _edited = edited;
}

QDateTime BaseModel::created()
{
    return _created;
}

void BaseModel::setCreated(QDateTime created)
{
    _created = created;
}

QDateTime BaseModel::updated()
{
    return _updated;
}

void BaseModel::setUpdated(QDateTime updated)
{
    _updated = updated;
}
