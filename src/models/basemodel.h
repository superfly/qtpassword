#ifndef BASEMODEL_H
#define BASEMODEL_H

#include <QDateTime>
#include <QString>
#include <QJsonObject>
#include <QSqlRecord>
#include <QMap>
#include <QVariant>

class BaseModel
{
public:
    BaseModel();
    BaseModel(QJsonObject json);
    BaseModel(QSqlRecord record);

    QMap<QString, QVariant> toMap();
    QVariant operator[](const QString &key);

    QString id();
    void setId(QString id);
    QString revision();
    void setRevision(QString revision);
    QString label();
    void setLabel(QString label);
    QString cseType();
    void setCseType(QString cseType);
    QString cseKey();
    void setCseKey(QString cseKey);
    QString sseType();
    void setSseType(QString sseType);
    QString client();
    void setClient(QString client);
    bool isHidden();
    void setHidden(bool isHidden);
    bool isTrashed();
    void setTrashed(bool isTrashed);
    bool isFavorite();
    void setFavorite(bool isFavorite);
    QDateTime edited();
    void setEdited(QDateTime edited);
    QDateTime created();
    void setCreated(QDateTime created);
    QDateTime updated();
    void setUpdated(QDateTime updated);

private:
    QString _id;
    QString _revision;
    QString _label;
    QString _cseType;
    QString _cseKey;
    QString _sseType;
    QString _client;
    bool _hidden;
    bool _trashed;
    bool _favorite;
    QDateTime _edited;
    QDateTime _created;
    QDateTime _updated;
};

#endif // BASEMODEL_H
