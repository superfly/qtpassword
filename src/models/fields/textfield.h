#ifndef TEXTFIELD_H
#define TEXTFIELD_H

#include "models/fields/customfield.h"

class TextField : public CustomField
{
public:
    TextField();
};

#endif // TEXTFIELD_H
