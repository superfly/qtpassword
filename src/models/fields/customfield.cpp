#include "customfield.h"

CustomField::CustomField()
{

}

QString CustomField::type()
{
    return _type;
}

QString CustomField::label()
{
    return _label;
}

void CustomField::setLabel(QString label)
{
    _label = label;
}

QString CustomField::value()
{
    return _value;
}

void CustomField::setValue(QString value)
{
    _value = value;
}
