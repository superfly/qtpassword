#ifndef URLFIELD_H
#define URLFIELD_H

#include "models/fields/customfield.h"

class UrlField : public CustomField
{
public:
    UrlField();
};

#endif // URLFIELD_H
