#ifndef CUSTOMFIELD_H
#define CUSTOMFIELD_H

#include <QString>

class CustomField
{
public:
    CustomField();

    QString type();
    QString label();
    void setLabel(QString label);
    QString value();
    void setValue(QString value);

protected:
    QString _type;

private:
    QString _label;
    QString _value;
};

#endif // CUSTOMFIELD_H
