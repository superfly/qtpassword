#ifndef SECRETFIELD_H
#define SECRETFIELD_H

#include "models/fields/customfield.h"

class SecretField : public CustomField
{
public:
    SecretField();
};

#endif // SECRETFIELD_H
