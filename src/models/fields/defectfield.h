#ifndef DEFECTFIELD_H
#define DEFECTFIELD_H

#include "models/fields/customfield.h"

class DefectField : public CustomField
{
public:
    DefectField();
};

#endif // DEFECTFIELD_H
