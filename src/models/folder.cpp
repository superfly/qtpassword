#include "folder.h"
#include "basemodel.h"

Folder::Folder() : BaseModel()
{

}

Folder::Folder(QJsonObject json) : BaseModel(json)
{
    if (json.contains("parent"))
    {
        setParent(json["parent"].toString());
    }
}

Folder::Folder(QSqlRecord record) : BaseModel(record)
{
    if (record.contains("parent"))
    {
        setParent(record.value("parent").toString());
    }
}

QMap<QString, QVariant> Folder::toMap()
{
    QMap<QString, QVariant> map = BaseModel::toMap();
    map["parent"] = QVariant(parent());
    return map;
}

QString Folder::parent()
{
    return _parent;
}

void Folder::setParent(QString parent)
{
    _parent = parent;
}
