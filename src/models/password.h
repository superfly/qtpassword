#ifndef PASSWORD_H
#define PASSWORD_H

#include <QVector>
#include <QSqlRecord>
#include "basemodel.h"
#include "fields/customfield.h"

class Password: public BaseModel
{
public:
    Password();
    Password(QSqlRecord record);
    Password(QJsonObject json);

    QMap<QString, QVariant> toMap();
    QString folder();
    void setFolder(QString folder);
    QString username();
    void setUsername(QString username);
    QString password();
    void setPassword(QString password);
    QString url();
    void setUrl(QString url);
    QString notes();
    void setNotes(QString notes);
    int status();
    void setStatus(int status);
    QString statusCode();
    void setStatusCode(QString statusCode);
    QString hash();
    void setHash(QString hash);
    QString share();
    void setShare(QString share);
    bool isShared();
    void setShared(bool isShared);
    bool isEditable();
    void setEditable(bool isEditable);

private:
    QString _revision;
    QString _username;
    QString _password;
    QString _url;
    QString _notes;
    QVector<CustomField*> _customFields;
    int _status;
    QString _statusCode;
    QString _hash;
    QString _folder;
    QString _share;
    bool _shared;
    bool _editable;
};

#endif // PASSWORD_H
