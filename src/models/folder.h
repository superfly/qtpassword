#ifndef FOLDER_H
#define FOLDER_H

#include <QVector>
#include <QString>
#include <QJsonObject>
#include <QSqlRecord>
#include "models/basemodel.h"

class Folder: public BaseModel
{
public:
    Folder();
    Folder(QSqlRecord record);
    Folder(QJsonObject json);

    QMap<QString, QVariant> toMap();

    QString parent();
    void setParent(QString parent);

private:
    QString _parent;
};

#endif // FOLDER_H
