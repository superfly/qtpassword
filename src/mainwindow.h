#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidgetItem>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QSettings>
#include <QUrl>
#include <QAction>

#include "database.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void onRequestFinished(QNetworkReply *reply);
    void checkLoginPoll();
    void hidePasswordsMessage();
    void on_searchEdit_textChanged(const QString &text);
    void on_loginButton_clicked();
    void on_sectionsTreeWidget_itemSelectionChanged();
    void on_passwordTreeWidget_itemActivated(QTreeWidgetItem *item, int column);
    void on_aboutAction_triggered();
    void on_exitAction_triggered();
    void on_logoutAction_triggered();
    void on_showPasswordButton_clicked();
    void on_copyLinkButton_clicked();
    void on_refreshPasswordsAction_triggered();
    void on_settingsAction_triggered();
    void on_settingsButtonBox_rejected();
    void on_lockAutomaticallyCheckBox_stateChanged(int state);
    void on_settingsButtonBox_accepted();
    void on_editButtonBox_accepted();
    void on_editButtonBox_rejected();

    void on_passwordTreeWidget_itemSelectionChanged();

private:
    Ui::MainWindow *ui;
    QNetworkAccessManager *netManager;
    QNetworkReply *currentReply;
    QWidget *toolbarSpacer;
    QLineEdit *searchEdit;
    Database *database;
    QList<Filter> *activeFilters;

    bool isLoggedIn = false;
    bool isRefreshing = false;
    QSettings settings;
    QUrl serverUrl;
    QUrl loginUrl;
    QUrl pollEndpoint;
    QByteArray pollToken;
    QByteArray sessionId;

    void updateActions();
    QNetworkRequest createRequest(QUrl url, QString path = QString(), bool isJson = false);
    QNetworkRequest createLoginRequest(QUrl url);
    bool hasCredentials();
    QByteArray encodeCredentials();
    void startLogin();
    void updatePasswordsFromServer();
    void clearPasswords();
    void loadPasswords();
    void showLoginError(QString errorText);
    void showPasswordsInfo(QString infoText);
    void showPasswordsError(QString errorText);
    void handleLogin(QNetworkReply *reply);
    void handlePoll(QNetworkReply *reply);
    void handleRequestSession(QNetworkReply *reply);
    void handleOpenSession(QNetworkReply *reply);
    void handleGetFolders(QNetworkReply *reply);
    void populateFolderChildren(QTreeWidgetItem *parentItem, QString parentFolder);
    void populateFolders();
    void setEditFields(QString title, QString username, QString password, QString url, QString notes);
};
#endif // MAINWINDOW_H
