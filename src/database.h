#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QtSql>
#include <QJsonObject>
#include <QList>
#include <QMap>
#include <QString>
#include <QVariant>

#include "models/folder.h"
#include "models/password.h"

class Filter
{
private:
    QString _field;
    QString _op;
    QVariant _value;

public:
    Filter(QString field = "", QVariant value = QVariant(), QString op = "=");
    QString field();
    void setField(QString field);
    QVariant value();
    void setValue(QVariant value);
    QString op();
    void setOp(QString op);
};

class Database : public QObject
{
    Q_OBJECT
private:
    QString _dbtype;
    QString _error;
    bool insertRecord(QString tableName, QMap<QString, QVariant> record);
    bool updateRecord(QString tableName, QMap<QString, QVariant> record);

public:
    Database(QString dbtype = "QSQLITE");
    bool initialize();
    QString error();
    void commit();
    bool exists(QString tableName, QString id);
    QList<Folder> getFolders(QString parent = "00000000-0000-0000-0000-000000000000");
    QList<Password> getPasswords(QList<Filter> &filters);
    Password getPassword(QString id);
    bool saveFolder(Folder &folder);
    bool savePassword(Password &password);
};

#endif // DATABASE_H
