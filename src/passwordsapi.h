#ifndef PASSWORDSAPI_H
#define PASSWORDSAPI_H

#include <QString>

namespace API
{

/**
 * @brief The base URL for the Nextcloud passwords app
 */
const QString BASE_URL = "/index.php/apps/passwords";

namespace Session
{
    /**
     * @brief Request the information to open a session
     *
     * URL: /api/1.0/session/request
     * Method: GET
     * Session required: no
     */
    const QString REQUEST = "/api/1.0/session/request";

    /**
     * @brief Open a new session
     *
     * URL: /api/1.0/session/open
     * Method: POST
     * Session required: no
     */
    const QString OPEN = "/api/1.0/session/open";

    /**
     * @brief Close an existing session
     *
     * URL: /api/1.0/session/close
     * Method: GET
     * Session required: no
     */
    const QString CLOSE = "/api/1.0/session/close";

    /**
     * @brief Keep the session alive
     *
     * URL: /api/1.0/session/keepalive
     * Method: GET
     * Session required: no
     */
    const QString KEEP_ALIVE = "/api/1.0/session/keepalive";
}

namespace Password
{
    /**
     * @brief List all passwords with the default detail level
     *
     * URL: /api/1.0/password/list
     * Method: GET/POST
     * Session required: yes
     */
    const QString LIST = "/api/1.0/password/list";

    /**
     * @brief Show a password
     *
     * URL: /api/1.0/password/show
     * Method: POST
     * Session required: yes
     */
    const QString SHOW = "/api/1.0/password/show";

    /**
     * @brief Find passwords matching given criteria
     *
     * URL: /api/1.0/password/find
     * Method: POST
     * Session required: yes
     */
    const QString FIND = "/api/1.0/password/find";

    /**
     * @brief Create a new password
     *
     * URL: /api/1.0/password/create
     * Method: POST
     * Session required: yes
     */
    const QString CREATE = "/api/1.0/password/create";

    /**
     * @brief Update an existing password
     * URL: /api/1.0/password/update
     * Method: PATCH
     * Session required: yes
     */
    const QString UPDATE = "/api/1.0/password/update";

    /**
     * @brief Delete a password
     *
     * URL: /api/1.0/password/delete
     * Method: DELETE
     * Session required: yes
     */
    const QString DELETE = "/api/1.0/password/delete";

    /**
     * @brief Restore an earlier state of a password
     *
     * URL: /api/1.0/password/restore
     * Method: PATCH
     * Session required: yes
     */
    const QString RESTORE = "/api/1.0/password/restore";

    /**
     * @brief Enumeration of password statuses
     */
    enum PasswordStatus {
        GOOD = 0,
        DUPLICATE = 1,
        BREACHED = 2,
        UNKNOWN = 3
    };
}

namespace Folder
{
    /**
     * @brief List all folders with the default/given detail level
     *
     * URL: /api/1.0/folder/list
     * Method: GET/POST
     * Session required: yes
     */
    const QString LIST = "/api/1.0/folder/list";

    /**
     * @brief Show a folder
     *
     * URL: /api/1.0/folder/show
     * Method: POST
     * Session required: yes
     */
    const QString SHOW = "/api/1.0/folder/show";

    /**
     * @brief Find folders matching given criteria
     *
     * URL: /api/1.0/folder/find
     * Method: POST
     * Session required: yes
     */
    const QString FIND = "/api/1.0/folder/find";

    /**
     * @brief Create a new folder
     *
     * URL: /api/1.0/folder/create
     * Method: POST
     * Session required: yes
     */
    const QString CREATE = "/api/1.0/folder/create";

    /**
     * @brief Update an existing folder
     *
     * URL: /api/1.0/folder/update
     * Method: PATCH
     * Session required: yes
     */
    const QString UPDATE = "/api/1.0/folder/update";

    /**
     * @brief Delete a folder
     *
     * URL: /api/1.0/folder/delete
     * Method: DELETE
     * Session required: yes
     */
    const QString DELETE = "/api/1.0/folder/delete";

    /**
     * @brief Restore an earlier state of a folder
     *
     * URL: /api/1.0/folder/restore
     * Method: PATCH
     * Session required: yes
     */
    const QString RESTORE = "/api/1.0/folder/restore";
}

}
#endif // PASSWORDSAPI_H
